# -*- coding: utf-8 -*-
import calendar
import datetime
import logging
import logging.config
import multiprocessing
import subprocess
import sys
from base64 import b64encode

import os
import pika
import re
import yaml
from flask import Blueprint, session
from flask import Flask, Response, flash, current_app
from flask import redirect, render_template, request, url_for
from flask.ext.login import LoginManager, login_required, current_user
from flask.ext.login import login_user, logout_user
from flask.ext.sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_paginate import Pagination
from flask_sslify import SSLify
from sqlalchemy import or_, and_
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.utils import secure_filename

from forms import LoginForm, AddServerForm, AddChange, ModifyServerForm, Incident, SearchServerForm, ReportGenForm, \
    SearchStoreLog
from novell_libraries import config
from novell_libraries.config import ALLOWED_EXTENSIONS, UPLOAD_FOLDER, DEBUG, ECHO, CMD_RUBY, USERS_FILE
from novell_libraries.dashboard_utils import Utils, UploadInfoFile, CreateReport, DatabaseExport, Logs
from novell_libraries.models import Base, User, servers, services_status, changes, incidents, downtime_logbook, \
    logbook_store
from web_reciber import WebReceiver

"""
    :author: Jesus Becerril Navarrete
    :organization: NOVELL MICROFOCUS
    :contact: jesus.becerril@microfocus.com

"""

__docformat__ = "restructuredtext"

message_progress = ""

app = Flask(__name__)

# app.wsgi_app = ProxyFix(app.wsgi_app)
sslify = SSLify(app, age=300, subdomains=True, permanent=True)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config.from_object(config)
app.config['SQLALCHEMY_ECHO'] = ECHO
app.config['SESSION_COOKIE_SECURE'] = True
app.config['SESSION_COOKIE_NAME'] = 'SLA_Reporteador'
app.debug = DEBUG
bootstrap = Bootstrap(app)
db = SQLAlchemy(app)
db.Model = Base
mod = Blueprint('data', __name__)
app.config.get('CSS_FRAMEWORK', 'bootstrap3')
app.jinja_env.add_extension('jinja2.ext.loopcontrols')
# CsrfProtect(app)
logging.config.dictConfig(yaml.load(open('/etc/dashboard/logging.conf')))
logfile = logging.getLogger('file')
logconsole = logging.getLogger('console')
logfile.debug("Debug FILE")
logconsole.debug("Debug CONSOLE")

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
queue_name = ""


@login_manager.user_loader
def load_user(user_id):
    return db.session.query(User).get(user_id)


@app.after_request
def apply_caching(response):
    response.headers["X-Frame-Options"] = "SAMEORIGIN"
    return response


@app.route('/')
def hello_world():
    return 'Its Work!'


@app.route('/login/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated():
        return redirect(url_for('create_report'))
    form = LoginForm(request.form)
    error = None
    if request.method == 'POST':
        user_name = str(form.user_name.data).lower().strip()
        password = str(form.password.data).lower().strip()
        user, authenticated = \
            User.authenticate(db.session.query, user_name, password)
        if authenticated:
            login_user(user)
            logger = Logs(USERS_FILE)
            logger.info_log("Se ha logeado el usuario: %s" % user_name)
            session['username'] = b64encode(os.urandom(8)).decode('utf-8')
            global queue_name
            queue_name = 'progress_' + str(session['username'])
            return redirect(url_for('create_report'))
        else:
            error = u'Nombre de usuario o contraseña incorrecta, prueba nuevamente.'
    return render_template('login.html', form=form, error=error)


@app.route('/logout/')
def logout():
    logout_user()
    session.pop('username', None)
    return redirect(url_for('login'))


@app.route('/add_server/', methods=['GET', 'POST'])
@login_required
def add_server():
    form = AddServerForm(request.form)
    error = None
    if request.method == 'POST':
        host = form.hostname.data.lower().strip()
        ip = form.ip.data.lower().strip()
        environment = form.environment.data.lower().strip()
        ip_prod = form.ip_prod.data.lower().strip()
        platform = form.platform.data.lower().strip()
        service = form.service.data.lower().strip()
        status = form.status.data.lower().strip()
        port = form.port.data.lower().strip()
        bio = Utils.identity_validator(host)
        service_group = str(form.service_group).lower().strip()
        try:
            if ((ip != None) and (ip != "")) and ((host != None) and (host != "")):
                q = db.session.query(servers).filter(servers.hostname == host).one()
                flash("El servidor ya esta dado de alta")
                return render_template('server.html', form=form, error=error)
            else:
                error = "Debes llenar almenos el campo de hostname, la ip administrativa y la ip productiva para poder dar de alta un servidor."
                return render_template('server.html', form=form, error=error)
        except NoResultFound:
            if ((ip != None) and (ip != "")) and ((host != None) and (host != "")):
                server = servers(hostname=host, date_admission=str(datetime.datetime.now().strftime("%Y/%m/%d-%H:%M")),
                                 ip_admin=ip, biometric=bio, status=status, environment=environment,
                                 ip_prod=ip_prod, platform=platform, service=service, service_group=service_group)
                db.session.add(server)
                db.session.commit()
                DatabaseExport().csv_export(db, servers)
                flash("El servidor a  sido dado de alta exitosamente")
                return render_template('server.html', form=form, error=error)
            else:
                error = "Debes llenar almenos el campo de hostname, ip adminstrativa e ip productiva para poder dar de alta un servidor."
                return render_template('server.html', form=form, error=error)
    else:
        return render_template('server.html', form=form, error=error)


@app.route('/modify_server/', methods=['GET', 'POST'])
@login_required
def modify_server():
    form = ModifyServerForm(request.form)
    error = None
    q = db.session.query(servers).order_by(servers.id.asc()).all()
    host_names = []
    for server_data in q:
        host_names.append(str(server_data.hostname).strip().strip("\'"))
    if request.method == 'POST':
        try:
            host = form.hostname.raw_data[0]
        except:
            host = ""
        ip = form.ip.data.lower().strip()
        environment = form.environment.data.lower().strip()
        ip_prod = form.ip_prod.data.lower().strip()
        platform = form.platform.data.lower().strip()
        service = form.service.data.lower().strip()
        status = form.status.data.lower().strip()
        port = form.port.data.lower().strip()
        service_group = str(form.service_group).lower().strip()

        try:
            if ((ip != None) and (ip != "")) and ((host != None) and (host != "")):
                q = db.session.query(servers).filter(servers.hostname == host).one()
                if environment != None and environment != "":
                    q.environment = environment
                if ip_prod != None and ip_prod != "":
                    q.ip_prod = ip_prod
                if platform != None and platform != "":
                    q.platform = platform
                if platform != None and platform != "":
                    q.service = service
                if port != None and port != "":
                    q.port = port
                if ip != None and ip != "":
                    q.ip_admin = ip
                if status != None and status != "":
                    q.status = status
                if service_group != None and service_group != "":
                    q.service_group = service_group

                db.session.flush()
                db.session.commit()
                DatabaseExport().csv_export(db, servers)
                flash("Los datos del servidor han sido modificados exitosamente")
                return render_template('server_modify.html', form=form, error=error)
            else:
                error = "Debes llenar los campos de hostname, ip administrativa e ip productiva para poder dar de alta un servidor."
                return render_template('server_modify.html', form=form, error=error)

        except NoResultFound:
            return render_template('server_modify.html', form=form, error=error, h_names=host_names)
    else:
        return render_template('server_modify.html', form=form, error=error, h_names=host_names)


@app.route('/searching_server/')
@login_required
def searching_server():
    form = SearchServerForm(request.form)
    error = None
    q = db.session.query(servers).order_by(servers.id.asc()).all()
    host_names = []
    for server_data in q:
        host_names.append(str(server_data.hostname).strip().strip("\'"))
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        total = 1
        try:
            host = request.args.get('search_server')
        except:
            host = ""
        try:
            if ((host != None) and (host != "")):
                q = db.session.query(servers).filter(servers.hostname == host).all()
                pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                        search=False, format_total=True, format_number=True,
                                        recordname='Inventory Servers')
                return render_template('inventory.html', result=q, page=page, per_page=per_page, pagination=pagination)

            else:
                return render_template('server_search.html', form=form, error=error, h_names=host_names)

        except NoResultFound:
            pass
    else:
        pass


@app.route('/infra_console/')
@login_required
def infra_console():
    return render_template('infra.html')


@app.route('/changes_incident/')
@login_required
def changes_incident():
    return render_template('changes_incidentes.html')


@app.route('/change_console/', methods=['GET', 'POST'])
@login_required
def change_console():
    form = AddChange(request.form)
    error = None
    listing = []
    if request.method == 'POST':
        date_change = form.date_cab.data.lower().strip()
        reference = form.reference.data.strip()
        change_name = form.change_name.data.lower().strip()
        category = form.category.data.lower().strip()
        environment = form.environment.data
        platform = form.platform.data
        description = form.description.data
        in_charge = form.in_charge.data.strip()
        applicant = form.applicant.data.strip()
        for hname in form.listing.data:
            listing.append(str(hname.hostname))
        date_ini = form.date_initial.data.lower().strip()
        time_ini = form.time_ini.data.lower().strip()
        time_end = form.time_end.data.lower().strip()
        vobo = form.vobo.data
        notes = form.notes.data
        support = form.support.data.lower().strip()
        priority = form.priority.data.lower().strip()
        try:
            q = db.session.query(changes).filter(changes.reference == reference).one()
            flash(u"Este cambio ya ha sido dado de alta ")
            return render_template('changes.html', form=form, error=error)
        except NoResultFound:
            change = changes(date_now=datetime.datetime.now().strftime("%d-%m-%Y-%H:%M"), datecab=date_change,
                             reference=reference, change_name=change_name, category=category, environment=environment,
                             platform=platform, description=description, in_charge=in_charge, applicant=applicant,
                             listing=listing, date_ini=date_ini, time_ini=time_ini, time_end=time_end, vobo=vobo,
                             notes=notes, support=support, priority=priority)

            db.session.add(change)
            db.session.commit()
            flash(u"La información del cambio se a guardado correctamente ")
            return render_template('changes.html', form=form, error=error)
    else:
        return render_template('changes.html', form=form, error=error)


@app.route('/incident/', methods=['GET', 'POST'])
@login_required
def incident_console():
    form = Incident(request.form)
    error = None
    listing = []
    if request.method == 'POST':
        date_incident = form.date_incident.data.lower().strip()
        reference = form.reference.data.lower().strip()
        incident_name = form.incident_name.data.lower().strip()
        environment = form.environment.data
        platform = form.platform.data
        incident_description = form.description.data.lower().strip()
        for hname in form.listing.data:
            listing.append(str(hname.hostname))
        time_ini = form.time_ini.data.lower().strip()
        time_end = form.time_end.data.lower().strip()
        notes = form.notes.data.lower().strip()

        try:
            q = db.session.query(incidents).filter(incidents.reference == reference).one()
            flash(u"Este cambio ya ha sido dado de alta ")
            return render_template('incidents.html', form=form, error=error)
        except NoResultFound:
            incident = incidents(date_now=datetime.datetime.now().strftime("%d-%m-%Y-%H:%M"), datecab=date_incident,
                                 reference=reference, incident_name=incident_name, environment=environment,
                                 platform=platform, description=incident_description, listing=listing,
                                 time_ini=time_ini, time_end=time_end, notes=notes)
            db.session.add(incident)
            db.session.commit()
            flash(u"La información del incidente se a guardado correctamente ")
            return render_template('incidents.html', form=form, error=error)

    else:
        return render_template('incidents.html', form=form, error=error)


@app.route('/report/')
@login_required
def create_report():
    return render_template('index.html')


@app.route('/servers_status/', methods=['POST'])
def service_status():
    if request.method == 'POST' and request.headers['Content-Type'] == 'application/json':
        js = request.json
        resp = WebReceiver().validate_web(js)
        return Response(status=resp[0], mimetypes=resp[1])


@app.route('/inventory/', methods=['GET'])
@login_required
def inventory():
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        q0 = db.session.query(servers).order_by(servers.id.asc()).all()
        q = db.session.query(servers).order_by(servers.id.asc()).limit(per_page).offset(offset).all()
        total = len(q0)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                search=False, format_total=True, format_number=True,
                                recordname='Inventory Servers')

        return render_template('inventory.html', result=q, page=page, per_page=per_page, pagination=pagination)


@app.route('/prod_inventory/', methods=['GET'])
@login_required
def prod_inventory():
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        q0 = db.session.query(servers).filter(
            or_(servers.environment == u"producción", servers.environment == u"produccion")).order_by(
            servers.id.asc()).all()
        q = db.session.query(servers).filter(
            or_(servers.environment == u"producción", servers.environment == u"produccion")).order_by(
            servers.id.asc()).limit(per_page).offset(offset).all()
        total = len(q0)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                search=False, format_total=True, format_number=True,
                                recordname='Inventory Servers')

        return render_template('inventory.html', result=q, page=page, per_page=per_page, pagination=pagination)


@app.route('/uat_inventory/', methods=['GET'])
@login_required
def uat_inventory():
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        q0 = db.session.query(servers).filter(servers.environment == u"uat").order_by(servers.id.asc()).all()
        q = db.session.query(servers).filter(servers.environment == u"uat").order_by(servers.id.asc()).limit(
            per_page).offset(offset).all()
        total = len(q0)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                search=False, format_total=True, format_number=True,
                                recordname='Inventory Servers')
        return render_template('inventory.html', result=q, page=page, per_page=per_page, pagination=pagination)


@app.route('/stress_inventory/', methods=['GET'])
@login_required
def stress_inventory():
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        q0 = db.session.query(servers).filter(servers.environment == u"stress").order_by(servers.id.asc()).all()
        q = db.session.query(servers).filter(servers.environment == u"stress").order_by(servers.id.asc()).limit(
            per_page).offset(offset).all()
        total = len(q0)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                search=False, format_total=True, format_number=True,
                                recordname='Inventory Servers')

        return render_template('inventory.html', result=q, page=page, per_page=per_page, pagination=pagination)


@app.route('/monitoring/', methods=['GET'])
@login_required
def monitoring():
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        q0 = db.session.query(services_status).order_by(services_status.date.asc()).all()
        q = db.session.query(services_status).order_by(services_status.date.asc()).limit(per_page).offset(offset).all()
        total = len(q0)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                search=False, format_total=True, format_number=True,
                                recordname='Monitoring')

        return render_template('monitoring.html', result=q, page=page,
                               per_page=per_page, pagination=pagination)


@app.route('/change_inventory/', methods=['GET'])
@login_required
def changes_inventory():
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        qq = db.session.query(changes).all()
        q = db.session.query(changes).order_by(changes.date_now.asc()).limit(per_page).offset(offset).all()
        total = len(qq)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                search=False, format_total=True, format_number=True,
                                recordname='Changes Inventory')
        return render_template('change_inventory.html', result=q, page=page,
                               per_page=per_page, pagination=pagination)


@app.route('/incidents_inventory/', methods=['GET'])
@login_required
def incidents_inventory():
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        q0 = db.session.query(incidents).all()
        q = db.session.query(incidents).order_by(incidents.datecab.asc()).limit(per_page).offset(offset).all()
        total = len(q0)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                search=False, format_total=True, format_number=True,
                                recordname='Incidents Inventory')

        return render_template('incidence_inventory.html', result=q, page=page,
                               per_page=per_page, pagination=pagination)


def allowed_file(filename):
    for i in ALLOWED_EXTENSIONS:
        if re.match(filename.rsplit('.', 1)[1], i.strip()):
            return True
    return False


@app.route('/upload_servers/', methods=['GET', 'POST'])
@login_required
def upload_servers():
    error = None
    up = UploadInfoFile()
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            try:
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                file_name = app.config['UPLOAD_FOLDER'] + filename
                up.upload_info(db, file_name, servers)
                DatabaseExport().csv_export(db, servers)
                flash("El archivo se proceso correctamente")
                return render_template('upload_server.html', error=error)
            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                flash("Hubo un problema al procesar el archivo %s %s %s " % (exc_type, exc_value, exc_traceback))
                return render_template('upload_server.html', error=error)
        else:
            flash("El archivo seleccionado no es un archivo valido para este sistema")
            return render_template('upload_server.html', error=error)
    else:
        return render_template('upload_server.html', error=error)


@app.route('/upload_log/', methods=['GET', 'POST'])
@login_required
def upload_log():
    error = None
    up = UploadInfoFile()
    if request.method == 'POST':
        try:
            file = request.files['file']
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                file_name = app.config['UPLOAD_FOLDER'] + filename
                up.upload_compress(db, file_name, logbook_store)

                flash("El archivo se proceso correctamente")
                return render_template('upload_log.html', error=error)
            else:
                flash("El archivo seleccionado no es un archivo valido para este sistema")
                return render_template('upload_log.html', error=error)

        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            flash("Hubo un problema al procesar el archivo %s %s %s " % (exc_type, exc_value, exc_traceback))
            return render_template('upload_log.html', error=error)
    else:
        return render_template('upload_log.html', error=error)


@app.route('/search_storelog/')
@login_required
def search_storelog():
    form = SearchStoreLog(request.form)
    error = None
    q = db.session.query(logbook_store).order_by(logbook_store.id.asc()).all()
    files_name = []
    for store_logs in q:
        files_name.append(str(store_logs.file_name).strip().strip("\'"))
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        total = 1
        try:
            file_storelog = request.args.get('search_log')
        except:
            file_storelog = ""
        try:
            if ((file_storelog != None) and (file_storelog != "")):
                q = db.session.query(logbook_store).filter(logbook_store.file_name == file_storelog).all()
                pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                        search=False, format_total=True, format_number=True,
                                        recordname='Inventory Store Logs')
                return render_template('inventory_store.html', result=q, page=page, per_page=per_page,
                                       pagination=pagination)

            else:
                return render_template('search_storelog.html', form=form, error=error, h_names=files_name)

        except NoResultFound:
            pass
    else:
        pass


@app.route('/search_plog/', methods=['GET', 'POST'])
@login_required
def search_plog():
    form = SearchStoreLog(request.form)
    error = None
    q = db.session.query(logbook_store).order_by(logbook_store.id.asc()).all()
    files_name = []
    for store_logs in q:
        files_name.append(str(store_logs.file_name).strip().strip("\'"))
    if request.method == 'GET':
        page, per_page, offset = get_page_items()
        total = 1
        try:
            file_storelog = request.args.get('search_log')
        except:
            file_storelog = ""
        try:
            if ((file_storelog != None) and (file_storelog != "")):
                q = db.session.query(logbook_store).filter(logbook_store.file_name == file_storelog).all()
                pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                        search=False, format_total=True, format_number=True,
                                        recordname='Inventory Store Logs')
                return render_template('inventory_store_process.html', result=q, page=page, per_page=per_page,
                                       pagination=pagination)

            else:
                return render_template('search_storelog.html', form=form, error=error, h_names=files_name)

        except NoResultFound:
            pass
    elif request.method == 'POST':
        store_file_md5 = request.form['option']
        store_data = db.session.query(logbook_store).filter(logbook_store.hash_md5 == store_file_md5).one()
        proc_dir = UploadInfoFile().extract_log(str(store_data.absolute_route),
                                                str(store_data.hash_md5), str(store_data.file_name))
        return redirect(url_for('proc_log', environment=store_data.environment, dir_log=proc_dir,
                                month=store_data.month, year=store_data.year))


@app.route('/process_environment/', methods=['GET', 'POST'])
@login_required
def process_environment():
    if request.method == 'GET':
        environment = request.args.get('proc_env')
        environment2 = request.args.get('proc_env2')
        page, per_page, offset = get_page_items()
        q0 = db.session.query(logbook_store).filter(
            or_(logbook_store.environment == environment, logbook_store.environment == environment2)).order_by(
            logbook_store.id.asc()).all()
        q = db.session.query(logbook_store).filter(
            or_(logbook_store.environment == environment, logbook_store.environment == environment2)).order_by(
            logbook_store.id.asc()).limit(per_page).offset(offset).all()
        total = len(q0)
        pagination = Pagination(page=page, per_page=per_page, total=total, css_framework='bootstrap3',
                                search=False, format_total=True, format_number=True,
                                recordname='Inventory store')

        return render_template('inventory_store_process.html', result=q, page=page, per_page=per_page,
                               pagination=pagination)
    elif request.method == 'POST':
        months = {'enero': 1, 'febrero': 2, 'marzo': 3, 'abril': 4, 'mayo': 5, 'junio': 6, 'julio': 7,
                  'agosto': 8, 'septiembre': 9, 'octubre': 10, 'noviembre': 11, 'diciembre': 12}

        store_file_md5 = request.form['option']
        store_data = db.session.query(logbook_store).filter(logbook_store.hash_md5 == store_file_md5).one()
        proc_dir = UploadInfoFile().extract_log(str(store_data.absolute_route),
                                                str(store_data.hash_md5), str(store_data.file_name))
        n_month = months[store_data.month]
        last_day = calendar.monthrange(int(store_data.year), int(n_month))[1]
        q = db.session.query(servers)
        q = q.filter(and_(or_(servers.status == "operativo", servers.status == "alta"),
                          servers.environment == store_data.environment))

        server = q.order_by(servers.id.asc()).all()
        dir_md5 = "/var/md5_files/"
        if os.path.exists(dir_md5):
            md5_file = "md5sum." + store_data.environment + "-" + store_data.month + store_data.year + ".txt"
            if os.path.exists(dir_md5 + md5_file):
                os.remove(dir_md5 + md5_file)
        else:
            os.makedirs(dir_md5)
            md5_file = "md5sum." + store_data.environment + "-" + store_data.month + store_data.year + ".txt"

        thread = multiprocessing.Process(target=run_ruby_command,
                                         args=(server, store_data.year, n_month, proc_dir, last_day))
        thread.start()

        return Response(status=200)


@app.route('/get_report/', methods=['GET', 'POST'])
@login_required
def get_report():
    error = None
    form = ReportGenForm(request.form)
    if request.method == 'POST':
        month = str(form.month.data)
        year = str(form.year.data)
        report = CreateReport()
        name = report.create_excel2(db, servers, services_status, changes, incidents, year, month, downtime_logbook)[0]
        return redirect(url_for('static', filename=name))
    else:
        return render_template('get_report.html', error=error, form=form)


@app.route('/progress/', methods=['GET', 'POST'])
@login_required
def progress():
    def generate():
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='localhost'))
        channel = connection.channel()
        channel.queue_declare(queue=queue_name)
        method_frame, header_frame, body = channel.basic_get(queue=queue_name)
        if method_frame:
            message = body
            channel.basic_ack(method_frame.delivery_tag)
            yield "data:" + str(message) + "\n\n"
            if int(body) == 100:
                message = 0
                channel.queue_delete(queue=queue_name)

    return Response(generate(), mimetype='text/event-stream')


def get_page_items():
    page = int(request.args.get('page', 1))
    per_page = request.args.get('per_page')
    if not per_page:
        per_page = current_app.config.get('PER_PAGE', 30)
    else:
        per_page = int(per_page)

    offset = (page - 1) * per_page
    return page, per_page, offset


def run_ruby_command(server, year, n_month, dir_log, last_day):
    toolbar_width = (int(len(server)) - 1)
    counter = 0
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    for i in server:
        counter += 1
        percent = (counter * 100) / toolbar_width
        if percent in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
            channel.basic_publish(exchange='',
                                  routing_key=queue_name,
                                  body=str(percent),
                                  )
        cmd = CMD_RUBY + " -Y " + str(year) + str(" -M ") + str(n_month) \
              + " -D " + str(last_day) + " -H " + str(i.hostname) + " -d " + str(dir_log) + " -I " + str(i.id)

        output = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output.wait()
