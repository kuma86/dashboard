from distutils.core import setup

setup(
    name='novell_libraries',
    version='1.0',
    packages=['novell_libraries'],
    url='www.novell.com',
    license='BSD',
    author='Jesus Becerril Navarrete',
    author_email='jesus.becerril@novell.com',
    description='Novell libraries from reports manager'
)
